
# scJuxtapose

[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![minimal R version](https://img.shields.io/badge/R%3E%3D-4.1.2-6666ff.svg)](https://cran.r-project.org/)
[![Snakemake](https://img.shields.io/badge/snakemake-≥6.4.1-brightgreen.svg?style=flat)](https://snakemake.readthedocs.io)

[Snakemake](https://snakemake.readthedocs.io/en/stable/) pipeline for running/benchmarking various single-cell multimodal integration methods. Currently supports such single-cell modalities: gene expression (RNA), chromatin accessibility (ATAC) and surface proteins (ADT). 

List of supported integration methods: 
- [Weighted Nearest Neighbor Analysis (WNN)](https://www.sciencedirect.com/science/article/pii/S0092867421005833)



## Workflow diagram

```mermaid
graph TD;
  a1["RNA preprocessing R"]-->b["mergeSeurat"];
  a2["ATAC preprocessing R"]-->b["mergeSeurat"];
  a3["ADT preprocessing R"]-->b["mergeSeurat"];
  b["mergeSeurat"]-->c1["WNN: RNA_ATAC"];
  b["mergeSeurat"]-->c2["WNN: RNA_ADT"];
  b["mergeSeurat"]-->c3["WNN: ATAC_ADT_RNA"];
  b["mergeSeurat"]-->c4["WNN: ATAC_ADT"];
```


## Input files


Currently supports sparse matrices as input for RNA and ATAC, and normal matrix as input for ADT. 

### Test data 
As test data I've prepared data from [TEA-seq](https://elifesciences.org/articles/63632) experiment on PBMC cells from GEO with accession number [GSM5123951](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM5123951). 

## Quick start

### 1. Clone repository

Clone or download current repo and go to the directory. 

```shell
git clone scjuxtapose.git
cd scjuxtapose
```

### 2. Edit [config file](config.json) 

Manually edit config file to specify path to your input data and change relevant analysis parameters. Detailed description of all parameters in the config file [is provided below](#configuration-file-parameters). 

List of **must change** parameters in the [config file](config.json):

### `par_general` section
- `outdir` - specify output folder absolute path
- `modalities` - list of modalities used for the analysis(currently supports: ATAC, RNA and ADT)

### Modalities sections (`RNA`, `ATAC` and `ADT`)
Depending on the modalities provided change input pathes to the entry files (`input_mtx`, `input_barcodes`, `input_genes` and `input_peaks`). Also double check `feature_column` parameter specific for `ATAC` and `RNA` section, as well as `genome` parameter specific for `ATAC` section (currently supports only hg38). 

### `WNN` section
- `combinations` - combinations of modalities used for integration with WNN. If not specified, use all possible combinations of `modalities` from `par_general` section. 

### 3. Execute pipeline 

> In order to let singularity see input/output files on the cluster we need to specify `--singularity-args` parameter in the *runAnalysis.sh* or *submit* file. By default it is "--bind /path/to/folder" and it needs to be changed to your home directory path or the root directory of the analysis folder containing both input and output files. Use output of `echo $HOME` on the cluster and replace `/home` with it. 

- Execute pipeline and run snakemake on a local node

```shell
sh runAnalysis.sh
```

- Start single snakemake job on the cluster

```shell
bsub < submit
```

<p>
<details>
<summary> Check [cluster file](cluster.json) to adapt running jobs LSF clusters </summary>

By default all jobs generated by this pipeline will be submitted to the LSF cluster with the parameters specified by the `default` section in the [cluster file](cluster.json). Depending on the size of input data user might need to change `memory` prerequisities (default: 15 Gb) and cluster `queue`. Important to note that one can specify specific cluster parameters for each Snakemake rule (`runRNA`, `runATAC`, `runADT`, `mergeSeurat` and `WNN`) and add to cluster file, as shown below. 

<pre><code>
{
    "__default__": {
    "queue": "short",
    "nCPUs": "{threads}",
    "memory": 15000,
    "name": "{params.jobname}",
    "output": "{params.logdir}/{params.jobname}.out",
    "error": "{params.logdir}/{params.jobname}.err"
  },
  "runRNA": {
    "queue": "short",
    "memory": 50000
  }
}
</code></pre>


</details>
</p>



## Output files

This pipeline produces several output files with a specific folder structure. 

### Folder `plots`

Current directory contains dimensionality reduction plots and associated data (in **tsv** format) from the analysis of single modalities and requested multimodal combinations using various integration methods. Currently supports only **WNN** integration with Seurat. 

> Results from multimodal integration are named with the specific scheme: 
**{modalities combination}.{integration method}.{dimensionality reduction method}.pdf** (f.e. ATAC_ADT.WNN.UMAP.pdf)

Example final UMAP plot from WNN integration ATAC + ADT

<img src="/img/ATAC_ADT.WNN.UMAP.png"  width="480" height="320">

### Folder `objects`

In this directory are located intermediate data objects (e.g. R Seurat objects with **.rds** extension) for single data modalities, as well as merged data object used for multimodal data integration. 

### Folder `logs`
Contans logs and error files for each executed Snakemake rule and associated R scripts. 


## Configuration file parameters

### `par_general` section
- `outdir` - specify full path to the directory where you want to have pipeline results 
- `metadata` - metadata vector containing cell identifiers to use for the analysis. At the moment it is not used in the pipeline, but is kept for future alignment between modalities. 
- `modalities` - which modalities to use in the analysis. Currently supports **ATAC**, **RNA** and **ADT**. Provide any combination of the supported modalities with `,` as delimeter. 
- `modalities` - specify full path to the input folder where input data is located 

### `RNA` section
- `matrix_type` - type of the input matrix, currently supported only "sparse"
- `input_mtx` - absolute path to the count matrix file in .mtx format 
- `input_barcodes` - absolute path to the barcodes file (cell IDs)
- `input_genes` - absolute path to the feature file with gene ids
- `feature_column` - which column in the feature file to use as gene ids
- `dims_start` - which dimension to use as first for [RunUMAP()](https://satijalab.org/seurat/reference/runumap)
- `dims_end` - which dimension to use as last for [RunUMAP()](https://satijalab.org/seurat/reference/runumap)
- `neigbours` - how many neigbouring points to use in [RunUMAP()](https://satijalab.org/seurat/reference/runumap). Suggested: 30 
- `resolution` - resolution parameter for [FindClusters()](https://satijalab.org/seurat/reference/findclusters). Suggested range between 0.5 and 2
- `cluster_algorithm` - clustering algorithm for  [FindClusters()](https://satijalab.org/seurat/reference/findclusters): 1 = original Louvain algorithm; 2 = Louvain algorithm with multilevel refinement; 3 = SLM algorithm

### `ATAC` section
- `matrix_type` - type of the input matrix, currently supported only "sparse"
- `input_mtx` - absolute path to the count matrix file in .mtx format 
- `input_barcodes` - absolute path to the barcodes file (cell IDs)
- `input_peaks` - absolute path to the feature file with peak ids.
> Peak identifiers should be separted by `c(":","-")`. Example - `chr1:100400-100500`
- `feature_column` - which column in the feature file to use as peak ids
- `fragments` - absolute path to the fragments file. 
> Fragments file should be indexed. Use `tabix -p BED $fragments_file`
- `dims_start` - which dimension to use as first for [RunUMAP()](https://satijalab.org/seurat/reference/runumap). Suggested: 2, as 1st dimension is usually highly correlated with seq depth
- `dims_end` - which dimension to use as last for [RunUMAP()](https://satijalab.org/seurat/reference/runumap)
- `neigbours` - how many neigbouring points to use in [RunUMAP()](https://satijalab.org/seurat/reference/runumap). Suggested: 30 
- `resolution` - resolution parameter for [FindClusters()](https://satijalab.org/seurat/reference/findclusters). Suggested range between 0.5 and 2
- `cluster_algorithm` - clustering algorithm for  [FindClusters()](https://satijalab.org/seurat/reference/findclusters): 1 = original Louvain algorithm; 2 = Louvain algorithm with multilevel refinement; 3 = SLM algorithm
- `genome` - genome annotation (currently supported only **hg38**)
- `variation_cutoff` - minimal cutoff to be included in the VariablFeatures for [FindTopFeatures()](https://satijalab.org/signac/reference/findtopfeatures). Example: `q25` = top 75% peaks are VariableFeatures
- `TFIDF_method` - which TF-IDR method to use in [RunTFIDF()](https://satijalab.org/signac/reference/runtfidf?q=runtfidf)

### `ADT` section
- `input_mtx` - absolute path to the count matrix file with cellIDs as columns and tags as rownames. Currently supports only dense matrix
- `dims_start` - which dimension to use as first for [RunUMAP()](https://satijalab.org/seurat/reference/runumap)
- `dims_end` - which dimension to use as last for [RunUMAP()](https://satijalab.org/seurat/reference/runumap)
- `neigbours` - how many neigbouring points to use in [RunUMAP()](https://satijalab.org/seurat/reference/runumap). Suggested: 30 
- `resolution` - resolution parameter for [FindClusters()](https://satijalab.org/seurat/reference/findclusters). Suggested range between 0.5 and 2
- `cluster_algorithm` - clustering algorithm for  [FindClusters()](https://satijalab.org/seurat/reference/findclusters): 1 = original Louvain algorithm; 2 = Louvain algorithm with multilevel refinement; 3 = SLM algorithm

### `mergeSeurat` section
- `main_modality` - which modality to use as main for merging. Suggested: RNA

### `WNN` section
- `combinations` - which combinations of modalities to use for WNN analysis. If `all` make all possible combinations from the providede modalities in [`par_general`](#par_general-section). Otherwise individual combinations can be provided separated by `,`, f.e. `ATAC_RNA,ADT_RNA`
- `resolution` - resolution parameter for [FindClusters()](https://satijalab.org/seurat/reference/findclusters). Suggested range between 0.5 and 2
- `cluster_algorithm` - clustering algorithm for  [FindClusters()](https://satijalab.org/seurat/reference/findclusters): 1 = original Louvain algorithm; 2 = Louvain algorithm with multilevel refinement; 3 = SLM algorithm

