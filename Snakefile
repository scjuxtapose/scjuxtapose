############################################
# Libraries, versions, authors and license #
############################################

import os
import time
import itertools

__author__  = "Ivan Berest" 
__license__ = "MIT"

start = time.time()

#############
# FUNCTIONS #
#############
onstart:
    print("\n\n#################################\n#  Workflow started...   #")

# The onsuccess handler is executed if the workflow finished without error.
onsuccess:
    print("\n\n#################################\n#  Workflow finished, no error  #")
    print(                                       "# Check the FINAL_OUTPUT folder #\n#################################\n\n")
    print("\nRunning time in minutes: %s\n" % round((time.time() - start)/60,1))
# Else, the onerror handler is executed.
onerror:
    print("\n\n#####################\n# An error occurred #\n#####################\n\n")
    print("\nRunning time in minutes: %s\n" % round((time.time() - start)/60,1))



#############################
# DIRECTORIES AND VARIABLES #
#############################

configfile: "config.json"

OUTDIR = config["par_general"]["outdir"]
OBJDIR = OUTDIR + "/" + "objects"
PLOTDIR = OUTDIR + "/" + "plots"
LOGDIR = OUTDIR + "/" + "logs"

dir_scripts = "scripts/"

ruleDisplayMessage = "\n# START EXECUTING RULE #\n"

#########################################
#              RULES                    #
#########################################

# should ne here before rule all
def gerCombinationsModalities(combinations):
    """text"""
    if combinations == "all":
        cond = config["par_general"]["modalities"]
        output = list(filter(lambda x: len(x) > 1,sum([list(map(list, itertools.combinations(cond.split(","), i))) for i in range(len(cond.split(",")) + 1 )],[])))
        outList = ['_'.join(i) for i in output]
    elif len(combinations) > 0:
        outList = combinations.split(",")
    else:
        raise KeyError("Could not find combinations of modalities for WNN analysis")
    return outList



INPUTDiR = config["par_general"]["inputDir"]

rule all:
    input:
        outAlluvial = PLOTDIR + "/" + "Comb.alluvial.pdf"

# singularity container
container: ""

rule runRNA:
    input:
        matrix = INPUTDiR + "/" + config["RNA"]["input_mtx"],
        barcodes = INPUTDiR + "/" + config["RNA"]["input_barcodes"],
        genes = INPUTDiR + "/" + config["RNA"]["input_genes"],
        metadata = INPUTDiR + "/" + config["par_general"]["metadata"]
    params:
        matrix_type = config["RNA"]["matrix_type"],
        feature_column = config["RNA"]["feature_column"],
        dims_start = config["RNA"]["dims_start"],
        dims_end = config["RNA"]["dims_end"],
        nn = config["RNA"]["neigbours"],
        resolution = config["RNA"]["resolution"],
        clustering_algorithm = config["RNA"]["cluster_algorithm"],
        jobname = "Initial_RNA",
        logdir = LOGDIR
    threads: 4
    output:
        RNA_rds = OBJDIR + "/" + "RNA.Seurat.rds",
        RNA_UMAP = PLOTDIR + "/" + "UMAP.RNA.pdf"
    log: expand('{dir}/runRNA.R.log', dir = LOGDIR)
    message: "{ruleDisplayMessage}Run first RNA analysis..."
    script: dir_scripts + "runRNA.R"

rule runATAC:
    input:
        matrix = INPUTDiR + "/" + config["ATAC"]["input_mtx"],
        barcodes = INPUTDiR + "/" + config["ATAC"]["input_barcodes"],
        peaks = INPUTDiR + "/" + config["ATAC"]["input_peaks"],
        fragments = INPUTDiR + "/" + config["ATAC"]["fragments"],
        metadata = INPUTDiR + "/" + config["par_general"]["metadata"]
    params:
        matrix_type = config["ATAC"]["matrix_type"],
        feature_column = config["ATAC"]["feature_column"],
        dims_start = config["ATAC"]["dims_start"],
        dims_end = config["ATAC"]["dims_end"],
        nn = config["ATAC"]["neigbours"],
        resolution = config["ATAC"]["resolution"],
        clustering_algorithm = config["ATAC"]["cluster_algorithm"],
        genome = config["ATAC"]["genome"],
        cutoff = config["ATAC"]["variation_cutoff"],
        method_TFIDF = config["ATAC"]["TFIDF_method"],
        jobname = "Initial_ATAC",
        logdir = LOGDIR
    threads: 4
    output:
        ATAC_rds = OBJDIR + "/" + "ATAC.Seurat.rds",
        ATAC_UMAP = PLOTDIR + "/" + "UMAP.ATAC.pdf",
        ATAC_cor = PLOTDIR + "/" + "ATAC.cor.PC.pdf"
    log: expand('{dir}/runATAC.R.log', dir = LOGDIR)
    message: "{ruleDisplayMessage}Run ATAC analysis..."
    script: dir_scripts + "runATAC.R"

rule runADT:
    input:
        matrix = INPUTDiR + "/" + config["ADT"]["input_mtx"],
        metadata = INPUTDiR + "/" + config["par_general"]["metadata"]
    params:
        dims_start = config["ADT"]["dims_start"],
        dims_end = config["ADT"]["dims_end"],
        nn = config["ADT"]["neigbours"],
        resolution = config["ADT"]["resolution"],
        clustering_algorithm = config["ADT"]["cluster_algorithm"],
        jobname = "Initial_ADT",
        logdir = LOGDIR
    threads: 4
    output:
        ADT_rds = OBJDIR + "/" + "ADT.Seurat.rds",
        ADT_UMAP = PLOTDIR + "/" + "UMAP.ADT.pdf"
    log: expand('{dir}/runADT.R.log', dir = LOGDIR)
    message: "{ruleDisplayMessage}Run initial ADT analysis..."
    script: dir_scripts + "runADT.R"


rule mergeSeurat:
    input:
        files = expand("{dir}/{mod}.Seurat.rds", dir = OBJDIR, mod = config["par_general"]["modalities"].split(",")),
        metadata = INPUTDiR + "/" + config["par_general"]["metadata"]
    params:
        mainmod = config["mergeSeurat"]["main_modality"],
        jobname = "Merge_Seurat",
        logdir = LOGDIR
    threads: 4
    output:
        DEBUG_file = LOGDIR + "/" + "mergeSeurat.debug.rds",
        mergedObj = OBJDIR + "/" + "merged.rds"
    log: expand('{dir}/mergeSeurat.log', dir = LOGDIR)
    message: "{ruleDisplayMessage} Merge single modalities into one Seurat object ..."
    script: dir_scripts + "mergeSeurat.R"



# somehow define what dims to take based on the input

rule WNN:
    input:
        mergedObj = OBJDIR + "/" + "merged.rds"
    params:
        comb = lambda wildcards: gerCombinationsModalities(config["WNN"]["combinations"]),
        resolution = config["WNN"]["resolution"],
        clustering_algorithm = config["WNN"]["cluster_algorithm"],
        jobname = expand('{{comb}}.WNN_combinations'),
        logdir = LOGDIR
    threads: 4
    output:
        outUMAP = expand('{dir}/{{comb}}.WNN.UMAP.pdf', dir = PLOTDIR),
        outTSV = expand('{dir}/{{comb}}.WNN.UMAP.tsv', dir = PLOTDIR)
    log: expand('{dir}/{{comb}}.WNN.log', dir = LOGDIR)
    message: "{ruleDisplayMessage} Run WNN analysis for combination {params.comb}..."
    script: dir_scripts + "WNN.R"



rule WNN_plot:
    input:
        outTSV = expand('{dir}/{comb}.WNN.UMAP.tsv', dir = PLOTDIR, comb = gerCombinationsModalities(config["WNN"]["combinations"]))
    params:
        comb = gerCombinationsModalities(config["WNN"]["combinations"])
    threads: 1
    output:
        outAlluvial = PLOTDIR + "/" + "Comb.alluvial.pdf",
        outVari = PLOTDIR + "/" + "UniqueClustersPer.pdf"
    log: expand('{dir}/WNN_plot.log', dir = LOGDIR)
    message: "{ruleDisplayMessage} Plot stat plots for combinations {params.comb}..."
    script: dir_scripts + "alluvial.R"

