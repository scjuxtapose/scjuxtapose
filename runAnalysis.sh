snakemake --configfile config.json \
--latency-wait 30 --notemp --rerun-incomplete --reason --keep-going \
--cores 4 --local-cores 1 --jobs 10 --cluster-config cluster.json \
--use-singularity --singularity-args "--bind /path/to/folder" \
--cluster "LSB_JOB_REPORT_MAIL=ERROR bsub -J {cluster.name} -q {cluster.queue}  \
    -n {cluster.nCPUs} -R \"span[hosts=1]\" \
   -M {cluster.memory} -o \"{cluster.output}\" \
   -e \"{cluster.error}\" "

